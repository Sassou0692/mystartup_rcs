	var changeTxt = function(str1, str2, str3) {
		document.getElementById('charh3').innerHTML = str1;
		document.getElementById('chardiv').innerHTML = str2;
		document.getElementById('charimg').src = str3;
	}


	document.getElementById('bers').addEventListener('click', function(e) {
	changeTxt("BERSERKER", "<p>ENOUGHT TALK ! GOLD AND GLORY AWAIT !</p><p><br>No heroes are more driven by a lust for adventure and a savage determination to win fame, fortune, and glory than the Berserkers. They wander the wild places of the world in search of formidable foes, fabulous treasures, and the sheer joy of a worthy challenge.</p><p><br>Possessed of an animalistic cunning and an unbridled fury, a Berserker is an untamed and unpredictable beast who is a blessing when set upon one's enemies—and a curse when turned against you.</p>", "img/Berserker.jpg");
	});

	document.getElementById('ember').addEventListener('click', function(e) {
		changeTxt("EMBERMAGE", "<p>EMBER IS DANGEROUS TO THE UNTRAINED HAND—AND DEADLY IN MINE.</p><p><br>At the forefront of the Empire’s colonization of Vilderan are the Embermages—highly-trained spellcasters renowned for their skill, dedication, and arcane power. No Imperial army is complete without these fierce warrior-wizards in the vanguard.</p><p><br>It is widely accepted in the Empire that Embermages are a special breed, held in awe and respect for the rigorous training they undergo, and the prowess they display. Selected in early adolescence by the Ember Council, Embermages undergo extensive arcane conditioning and mystical studies, accompanied by hours of physical drills and combat exercises each and every day.</p>", "img/Embermage.jpg");
	});

	document.getElementById('steamer').addEventListener('click', function(e) {
		changeTxt("STEAMER", "<p>IF A PROBLEM IS TOO BIG TO FIX...ONE MUST BREAK IT INTO SMALLER PROBLEMS.</p><p><br>The workhorse of the Empire, the modern Engineer’s strength lies in his Ember-fueled, steam-driven armor—a technological marvel devised by top scientists at the Industrium, and crafted personally by the Engineer as his journeyman project.</p><p><br>Engineers carry out any mission involving technology that the Empire requires, from building mighty Herkonian Embercraft transports to delving into ancient ruins in search of long-forgotten knowledge—sometimes as part of a team of Imperial soldiers, but more often than not as 'independent' contractors.</p>", "img/Engineer.jpg");
	});